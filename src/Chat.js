import React, {Fragment} from "react"
import {ChatMessage} from "./ChatMessage";

const URL = "wss://imr3-react.herokuapp.com";

export class Chat extends React.Component {
    static MYNAME = "Axel&Coco"

    constructor(props) {
        super(props);
        this.state = {
            ws: null,
            connected: false,
            messages: [],
            newMessage: ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.messagesEndRef = React.createRef()
    }

    componentDidMount() {
        this.ws = new WebSocket(URL)

        this.ws.onopen = () => {
            console.log("connected");
            this.setState({
                connected: true,
            });
        };

        this.ws.onmessage = evt => {
            const messages = JSON.parse(evt.data);
            messages.map(message => this.addMessage(message));
        };

        this.ws.onclose = () => {
            console.log("disconnected, reconnect.");
            this.setState({
                connected: false,
                ws: new WebSocket(URL)
            });
        };
    }

    addMessage = (message) => {
        this.setState({
            messages: [...this.state.messages, message],
        });
        this.scrollToBottom()
    }

    submitMessage = () => {
        const message = {name: Chat.MYNAME, message: this.state.newMessage};
        this.ws.send(JSON.stringify(message));
        this.setState({
            newMessage: "",
        });
    };

    handleChange(event) {
        this.setState({newMessage: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.submitMessage()
    }

    toStrDate = (timestamp) => {
        let date = new Date(timestamp);
        let hours = "0" + date.getHours()
        let minutes = "0" + date.getMinutes()
        return `${hours.slice(-2)}:${minutes.slice(-2)}`
    }

    scrollToBottom = () => {
        this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
    }


    render() {
        return (
            <Fragment>
                <div className={"chat-header"}>
                    <h2>
                        Chat
                    </h2>
                </div>
                <hr/>
                <div className={"chat-messages"} >
                    {this.state.messages.map((item, index) => (
                        <ChatMessage
                            key={index}
                            when={this.toStrDate(item["when"])}
                            name={item["name"]}
                            message={item["message"]}
                        />

                    ))}
                    <div ref={this.messagesEndRef} />
                </div>
                <div className={"chat-footer"}>
                    <form className={"form"} onSubmit={this.handleSubmit}>
                        <input type="text"
                               placeholder={"Message..."}
                               value={this.state.newMessage}
                               onChange={this.handleChange} />
                        <button>Envoyer</button>
                    </form>
                </div>
            </Fragment>
        );
    }
}
