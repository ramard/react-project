import React from "react"
import PropTypes from "prop-types";
import classNames from 'classnames'
import {Chat} from "./Chat";

export class ChatMessage extends React.Component {

    static propTypes = {
        when: PropTypes.string,
        name: PropTypes.string,
        message: PropTypes.string
    }

    render() {
        let className = classNames({
            extern: this.props.name !== Chat.MYNAME,
            self: this.props.name === Chat.MYNAME
        });

        return(
            <div className={"chat-message " + className}>
                <div className={"header"}>
                    <div className={"name"}>{this.props.name}</div>
                    <div className={"when"}>{this.props.when}</div>
                </div>
                <div className={"content"}>
                    <div className={"text"}>{this.props.message}</div>
                </div>
            </div>
        )
    }


}