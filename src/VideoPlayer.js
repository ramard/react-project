import React from 'react';
import PropTypes from "prop-types";
import "video-react/dist/video-react.css";
import { Player } from 'video-react';

export default class VideoPlayer extends React.Component {

    constructor(props) {
        super(props);
        this.player = null;
        this.seek = this.seek.bind(this);
    }

    static defaultProps = {
        poster: "../public/player_logo.png",
        src: "https://media.w3.org/2010/05/sintel/trailer_hd.mp4",
        seekTime: 0
    }

    static propsType = {
        poster: PropTypes.string,
        src: PropTypes.string,
        seekTime: PropTypes.number,
    }

    componentDidMount() {
        // subscribe state change
        this.player.subscribeToStateChange(this.handleStateChange.bind(this));
    }

    handleStateChange(state, prevState) {
        this.props.action(state.currentTime);
    }

    seek(seconds) {
        if (this.player != null) {
            this.player.seek(seconds);
        }
    }

    render() {
        this.seek(this.props.chapterTime);
        return (
            <Player
                autoPlay
                ref={player => {
                    this.player = player;
                }}
                playsInline
                poster="/assets/poster.png"
                src={this.props.src}
                // onChange={this.props.handleChange}
            />
        )
    }
}