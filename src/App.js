import logo from './logo.svg';
import './App.css';
import {Controller} from "./Controller";

function App() {
  return (
      <Controller/>
  );
}

export default App;
