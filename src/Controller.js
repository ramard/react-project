import React, {Fragment} from "react"
import VideoPlayer from "./VideoPlayer";
import {ListChapter} from "./ListChapter";
import {Map} from "./Map";
import {Chat} from "./Chat";
import {TagZone} from "./ListTag";

export class Controller extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data_loaded: false,
            data: {},
            timestamp: -1
        }
        this.listTagRef = React.createRef();
        this.chapterRef = React.createRef();
    }

    handleChapterClick = (chapter) => {
        this.setVideoTime(chapter.props.pos)
    }

    handleCurrentTime = (timestamp) => {
        this.listTagRef.current.setCurrentPos(timestamp);
        this.chapterRef.current.setCurrentPos(timestamp);
    }

    setVideoTime = (timestamp) => {
        this.setState({timestamp: timestamp})
    }

    componentDidMount() {
        fetch("https://imr3-react.herokuapp.com/backend")
            .then(res => res.json())
            .then(result => {
                this.setState({
                    data_loaded: true,
                    data: result
                });
            });
    }

    render() {
        const { data_loaded, data } = this.state;
        if (data_loaded) {
            const fileUrl = data["Film"]["file_url"]
            const title = data["Film"]["title"]

            return (
                <Fragment>
                    <div className={"video-container"}>
                        <div className="Video card">
                            <h2 className={"title"}>{title}</h2>
                            <VideoPlayer action={this.handleCurrentTime} src={fileUrl} chapterTime={this.state.timestamp}/>
                        </div>
                    </div>
                    <div className="masonry">
                        <div className="tag-zone card item">
                            <h2 className={"title"}>{"Tags"}</h2>
                            <TagZone
                                ref={this.listTagRef}
                                currentPos={this.state.timestamp}
                                data={data["Keywords"]}
                                fields={["pos", "data"]}
                                onClick={this.handleCurrentTime}
                            />
                        </div>
                        <div className="chat card item">
                            <Chat/>
                        </div>
                        <div className="list-chapter card item">
                            <h2 className={"title"}>{"Chapitres"}</h2>
                            <ListChapter
                                ref={this.chapterRef}
                                items={data["Chapters"]}
                                fields={["pos", "title"]}
                                onClick={this.handleChapterClick}/>
                        </div>
                        <div className="map card item">
                            <h2 className={"title"}>{"Map"}</h2>
                            <Map
                                dataParentToChild={data["Waypoints"]}
                                onClick={this.setVideoTime}
                            />
                        </div>
                    </div>
                </Fragment>
            )
        } else {
            return (
                <div>
                    <p>Loading data</p>
                </div>
            )
        }
    }

}