import React, {Fragment} from "react";
import PropTypes from "prop-types";

export class Tag extends React.Component {
    static defaultProps = {
        selected: false,
    }

    static propTypes = {
        title: PropTypes.string,
        url: PropTypes.string,
    }


    render() {
        return(
            <div className={"tag"}>
                <a target="_blank" href={this.props.url}>{this.props.title}</a>
            </div>
        )
    }
}

// liste de tags pour un timestamp donné
export class ListTag extends React.Component{
    static propTypes = {
        previousBound: PropTypes.number,
        nextBound: PropTypes.number,
        timestamp: PropTypes.number,
        data: PropTypes.array,
        fields: PropTypes.arrayOf(String).isRequired
    }

    render() {
        if((this.props.previousBound <= this.props.timestamp) && ((this.props.nextBound === Infinity) || (this.props.timestamp < this.props.nextBound))){
            return (
                <div className={"tags-list"}>
                    {
                        this.props.data.map((item, index) =>(
                            <Tag
                                key={index}
                                title={item[this.props.fields[0]]}
                                url={item[this.props.fields[1]]} />
                        ))
                    }
                </div>
            )
        }
        else{
            return null;
        }


    }
}

//affiche toutes les listes de tags
export class TagZone extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            timestamp: 0
        }
    }

    static propTypes = {
        data: PropTypes.array.isRequired,
        fields: PropTypes.arrayOf(String).isRequired,
    }

    setCurrentPos(currentTimestamp){
        this.setState({
            timestamp: currentTimestamp
        });
    }

    render() {
        return (
            <div>
                {this.props.data.map((item, index) => (
                    <ListTag
                        key={index}
                        previousBound={parseInt(item[this.props.fields[0]])}
                        nextBound={index !== this.props.data.length-1 ? parseInt(this.props.data[index + 1][this.props.fields[0]]) : Infinity}
                        timestamp={this.state.timestamp}
                        data={item[this.props.fields[1]]}
                        fields={["title", "url"]} />
                ))}
            </div>
        )
    }
}