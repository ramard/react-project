import React, {Fragment} from "react"
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet'
import PropTypes from "prop-types";

export class Map extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.dataParentToChild
        }
    }

    static propTypes = {
        onClick: PropTypes.func.isRequired
    }


    handleMarkerClick = (item) => {
        this.props.onClick(item.timestamp);
    }

    render() {
        const {data} = this.state;

        if (data != null) {
            return (
                <div>
                    <MapContainer center={[data[0].lat, data[0].lng]} zoom={3} scrollWheelZoom={false}>
                        <TileLayer
                            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        {data.map((item, index) => (
                            <Marker
                                key={index}
                                position={[item.lat, item.lng]}
                                eventHandlers={{
                                    click: this.handleMarkerClick.bind(this, item)
                                }}>
                                <Popup>
                                    {item.label}
                                </Popup>
                            </Marker>
                        ))}
                    </MapContainer>
                </div>
            )
        }
    }
}