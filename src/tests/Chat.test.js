import React from "react";
import {render, screen} from "@testing-library/react";
import {Chat} from "../Chat";
import {configure, mount} from "enzyme";
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import {ChatMessage} from "../ChatMessage";

configure({ adapter: new Adapter() });

const fakedata = [
    {
        "when": "12:45",
        "name": "Axel&Coco",
        "message": "Test 1"
    },
    {
        "when": "12:45",
        "name": "Axel&Coco",
        "message": "Test 2"
    }
]

test('Render component without crash', () => {
    render(<Chat/>);
})

test('Input text to send message is present', () => {
    render(<Chat/>);
    let element = screen.getByPlaceholderText("Message...");
    expect(element).toBeInTheDocument()
})

test('Button to send message is present', () => {
    render(<Chat/>);
    let element = screen.getByText("Envoyer");
    expect(element).toBeInTheDocument()
})

test('Add 2 messages', () => {
    const component = mount(<Chat/>);
    component.setState({
        messages: fakedata,
    });
    const messages = component.find(ChatMessage)
    expect(messages).toHaveLength(2);
})



