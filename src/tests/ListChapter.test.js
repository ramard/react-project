import React from "react";
import {fireEvent, render, screen} from "@testing-library/react";
import {ListChapter} from "../ListChapter";

const data = [
    {
        "pos": "0",
        "title": "Start"
    },
    {
        "pos": "45",
        "title": "Intro"
    }
]

const fields = ["pos", "title"];

let currentChapter = null;

function callback(chapter) {
    currentChapter = chapter;
}

test('Render component without crash', () => {
    render(<ListChapter items={data} fields={fields} onClick={callback.bind()}/>);
})

test('Validate selected chapter', () => {
    render(<ListChapter items={data} fields={fields} onClick={callback.bind()}/>);

    let element = screen.getByText("Start");
    fireEvent.click(element);

    expect(currentChapter.props.pos).toBe(parseInt(data[0]["pos"]));
    expect(currentChapter.props.title).toBe(data[0]["title"]);
})

test('Update selected chapter according to time', () => {
    let listChapterRef = React.createRef();
    render(<ListChapter ref={listChapterRef} items={data} fields={fields} onClick={callback.bind()}/>);

    // par défaut on est sur start
    let element = document.getElementsByClassName('on')[0]
    expect(element.textContent).toBe("Start");

    // on place le time au 2e chapitre pour vérifier que le chapitre selected est bien Intro
    listChapterRef.current.setCurrentPos(46);

    element = document.getElementsByClassName('on')[0]
    expect(element.textContent).toBe("Intro");
})