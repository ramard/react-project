import React from "react";
import {fireEvent, render, screen} from "@testing-library/react";
import {ListTag, TagZone} from "../ListTag";

const data = [
    {
        "pos": "0",
        "data": [
            {
                "title": "Route 66",
                "url": "https://en.wikipedia.org/wiki/U.S._Route_66"
            },
            {
                "title": "Stefan Kluge",
                "url": "http://www.imdb.com/name/nm1667631/"
            },
            {
                "title": "Mathias Einmann",
                "url": "http://www.imdb.com/name/nm1667578/"
            }
        ]
    },
    {
        "pos": "117",
        "data": [
            {
                "title": "New Mexico",
                "url": "https://en.wikipedia.org/wiki/New_Mexico"
            },
            {
                "title": "Cadillac",
                "url": "https://en.wikipedia.org/wiki/Cadillac_Series_62"
            }
        ]
    }]

const listFields = ["pos", "data"];
const tagFields = ["title", "url"];

let currentChapter = null;

function callback(chapter) {
    currentChapter = chapter;
}

test('Render component without crash', () => {
    render(<TagZone data={data} fields={listFields}/>);
})

test('Validate first tag', () => {
    let videoPlayerRef = React.createRef();
    render(<TagZone data={data} fields={listFields} ref={videoPlayerRef}/>);

    // tags par défaut => ceux qui sont présents à 0
    screen.getByText("Route 66");
    screen.getByText("Stefan Kluge");
    screen.getByText("Mathias Einmann");
})

test('Validate tag with set position', () => {
    let videoPlayerRef = React.createRef();
    render(<TagZone data={data} fields={listFields} ref={videoPlayerRef}/>);

    videoPlayerRef.current.setCurrentPos(117)

    screen.getByText("New Mexico");
    screen.getByText("Cadillac");
})