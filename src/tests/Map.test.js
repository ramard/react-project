import React from "react";
import {render, screen} from "@testing-library/react";
import {Map} from "../Map";
import {configure, mount} from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import {Marker} from "react-leaflet";

configure({ adapter: new Adapter() });

const fakedata = [
    {
        "lat": "32.42",
        "lng": "-90.13",
        "label": "Ridgeland",
        "timestamp": "45"
    },
    {
        "lat": "38.90",
        "lng": "-77.04",
        "label": "Washington DC",
        "timestamp": "300"
    },
]

function callback(chapter) {
    return null
}

test('Render component without crash', () => {
    render(<Map
        dataParentToChild={fakedata}
        onClick={callback}
    />);
})

test('Check if there are 2 Markers', () => {
    const component = mount(<Map
        dataParentToChild={fakedata}
        onClick={callback}
    />);
    const markers = component.find(Marker)
    expect(markers).toHaveLength(2);
})