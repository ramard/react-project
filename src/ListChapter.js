import React from "react";
import PropTypes from "prop-types";
import classNames from 'classnames'

export class Chapter extends React.Component {
    static defaultProps = {
        selected: false,
    }

    static propTypes = {
        pos: PropTypes.number,
        previousBound: PropTypes.number,
        nextBound: PropTypes.number,
        currentTime: PropTypes.number,
        title: PropTypes.string,
        onClick: PropTypes.func,
        selected: PropTypes.bool
    }

    toggle = () => {
        this.props.onClick(this);
    }

    render() {
        let className = classNames({
            listItem: true,
            on: this.props.previousBound <= this.props.currentTime && this.props.currentTime < this.props.nextBound,
            off: !(this.props.previousBound <= this.props.currentTime && this.props.currentTime < this.props.nextBound)
        });

        return(
            <div className={"chapter " + className} onClick={this.toggle}>
                <div>{this.props.title}</div>
            </div>)
    }
}

export class ListChapter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: -1,
            currentTime: 0
        }
    }

    handleClick = (index, chapter) => {
        this.setState({selected: index})
        this.props.onClick(chapter);
    }

    setCurrentPos(currentTimestamp){
        this.setState({
            currentTime: currentTimestamp
        });
    }

    static propTypes = {
        items: PropTypes.array.isRequired,
        fields: PropTypes.arrayOf(String).isRequired,
        onClick: PropTypes.func.isRequired,
        currentTime: PropTypes.number
    }

    render() {
        return (
            <ul>
                {this.props.items.map((item, index) => (
                    <Chapter
                        title={item[this.props.fields[1]]}
                        pos={parseInt(item[this.props.fields[0]])}
                        previousBound={parseInt(item[this.props.fields[0]])}
                        nextBound={index !== this.props.items.length-1 ? parseInt(this.props.items[index + 1][this.props.fields[0]]) : Infinity}
                        key={index}
                        onClick={this.handleClick.bind(this, index)}
                        currentTime={this.state.currentTime}
                    />
                ))}
            </ul>
        )
    }
}