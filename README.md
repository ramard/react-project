# Projet React 

## Prérequis
Pour que le projet fonctionne correctement et a été testé sur des versions de Node supérieures ou égale à `17.0`.

Avant de lancer de passer à une des étapes suivantes il faut installer les dépendances. 
```
npm install
```

## Lancer l'application 
Lancer la commande : 
```
npm start
```

## Lancer tous les tests unitaires
Lancer la commande :
```
npm test
```

---
Auteurs : Corentin BOULC'H, Axel RAMARD